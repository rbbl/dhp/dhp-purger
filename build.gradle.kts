import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.7.20"
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "cc.rbbl.dhp"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/28863171/packages/maven")
}

dependencies {
    implementation("cc.rbbl:program-parameters-jvm:1.0.3")
    implementation("dev.kord:kord-core:0.8.0-M17")
    implementation("ch.qos.logback:logback-classic:1.2.9")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("MainKt")
}

tasks.getByName<ShadowJar>("shadowJar") {
    archiveFileName.set("dhp-purger.jar")
}