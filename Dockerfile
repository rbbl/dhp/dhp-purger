FROM gradle:7.4.2-alpine as BUILDER
COPY src/ /app/src/
COPY gradle/ /app/gradle/
COPY gradle* /app/
COPY build.gradle.kts /app
WORKDIR /app
RUN gradle build

FROM eclipse-temurin:17-alpine
COPY --from=BUILDER /app/build/libs/dhp-purger.jar /app/dhp-purger.jar
ENTRYPOINT ["java", "-jar", "/app/dhp-purger.jar" ]