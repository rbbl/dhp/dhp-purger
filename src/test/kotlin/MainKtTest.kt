import org.junit.jupiter.api.Test
import java.time.Duration
import kotlin.test.assertEquals

class MainKtTest {

    @Test
    fun formatStartingMessage() {
        assertEquals("Cleaning up everything after 7d", formatStartingMessage("Cleaning up everything after {t}", "7d"))
    }

    @Test
    fun formatEndMessage() {
        assertEquals(
            "Done. Deleted 420 Messages in 1 hours 24 minutes and 30 seconds", formatEndMessage(
                "Done. Deleted {messageCounter} Messages in {hours} hours {minutes} minutes and {seconds} seconds",
                420,
                Duration.ofSeconds(3600 + 1440 + 30) //1h24m30s
            )
        )
    }
}