import dev.kord.common.entity.DiscordMessage
import dev.kord.common.entity.Snowflake
import dev.kord.rest.json.request.BulkDeleteRequest
import dev.kord.rest.route.Position
import dev.kord.rest.service.RestClient
import io.ktor.util.*
import kotlinx.datetime.Instant
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.ZoneOffset
import kotlin.system.measureNanoTime

object DiscordRestClient {
    lateinit var client: RestClient
}

const val limit = 50

suspend fun main(args: Array<String>) {
    ProgramConfig.parameters.loadParametersFromEnvironmentVariables()
    ProgramConfig.parameters.loadParameters(args)
    ProgramConfig.parameters.checkParameterCompleteness()

    val config = ProgramConfig()
    DiscordRestClient.client = RestClient(config.discordToken)

    val timeNumber = Regex("\\d+").find(config.maxAge)!!.value.toLong()
    val timeUnit = Regex("\\D{1}").find(config.maxAge)!!.value.toLowerCasePreservingASCIIRules()
    val deleteBefore = when (timeUnit) {
        "d" -> now().minusDays(timeNumber)
        "w" -> now().minusWeeks(timeNumber)
        "m" -> now().minusMonths(timeNumber)
        else -> throw IllegalArgumentException()
    }

    deleteMessagesBefore(config.channelId, deleteBefore)
}

suspend fun deleteMessagesBefore(channelId: Snowflake, before: LocalDateTime) {
    val config = ProgramConfig()
    var messageCounter = 0L
    DiscordRestClient.client.channel.createMessage(channelId) {
        content = formatStartingMessage(config.startingMessage, config.maxAge)
    }
    val durationNano = measureNanoTime {
        handleMessagesBefore(channelId, before) {
            messageCounter += it.size
            if (it.last().timestamp.isBefore(now().minusDays(13))) {
                it.forEach { message ->
                    DiscordRestClient.client.channel.deleteMessage(channelId, message.id)
                }
            } else {
                DiscordRestClient.client.channel.bulkDelete(channelId, BulkDeleteRequest(it.map { it.id }))
            }
        }
    }
    val duration = Duration.ofNanos(durationNano)
    DiscordRestClient.client.channel.createMessage(channelId) {
        content = formatEndMessage(config.endMessage, messageCounter, duration)
    }
}

suspend fun handleMessagesBefore(
    channelId: Snowflake,
    before: LocalDateTime,
    action: suspend (messages: List<DiscordMessage>) -> Unit
) {
    val firstMessage = getFirstMessageBefore(channelId, before) ?: return
    if (!firstMessage.timestamp.isBefore(before)) {
        return
    }
    var messages = DiscordRestClient.client.channel.getMessages(channelId, Position.Before(firstMessage.id), limit - 1)
    if (firstMessage.timestamp.isBefore(before)) {
        messages = listOf(firstMessage) + messages
    }
    if (messages.isEmpty()) {
        return
    }
    do {
        action(messages)
        if (messages.size == limit) {
            messages =
                DiscordRestClient.client.channel.getMessages(channelId, Position.Before(messages.last().id), limit)
        }
    } while (messages.size == limit)
}

suspend fun getFirstMessageBefore(channelId: Snowflake, after: LocalDateTime): DiscordMessage? {
    var messages = DiscordRestClient.client.channel.getMessages(channelId, limit = limit)
    do {
        messages.forEach {
            if (it.timestamp.epochSeconds < after.toEpochSecond(ZoneOffset.UTC)) {
                return it
            }
        }
        if (messages.size == limit) {
            messages =
                DiscordRestClient.client.channel.getMessages(channelId, Position.Before(messages.last().id), limit)
        }
    } while (messages.size == limit)
    return null
}

fun Instant.isBefore(dateTime: LocalDateTime): Boolean = this.epochSeconds < dateTime.toEpochSecond(ZoneOffset.UTC)

fun formatStartingMessage(input: String, maxAge: String) = input.replace("{t}", maxAge)


fun formatEndMessage(input: String, messageCount: Long, duration: Duration) =
    input.replace("{messageCounter}", messageCount.toString()).replace("{hours}", duration.toHours().toString())
        .replace("{minutes}", duration.toMinutesPart().toString())
        .replace("{seconds}", duration.toSecondsPart().toString())