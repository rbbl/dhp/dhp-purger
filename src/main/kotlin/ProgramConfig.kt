import cc.rbbl.program_parameters_jvm.ParameterDefinition
import cc.rbbl.program_parameters_jvm.ParameterHolder
import dev.kord.common.entity.Snowflake
import org.slf4j.LoggerFactory

class ProgramConfig {
    private val defaultStartingMessage = "Cleaning up everything after {t}."
    private val defaultEndMessage = "Done. Deleted {messageCounter} Messages in {hours} hours {minutes} minutes and {seconds} seconds."
    private val defaultMaxAge = "7d"
    private val log = LoggerFactory.getLogger(this::class.java)

    companion object {
        val parameters = ParameterHolder(
            setOf(
                ParameterDefinition("DISCORD_TOKEN"),
                ParameterDefinition("CHANNEL_ID"),
                ParameterDefinition("MAX_AGE", required = false),
                ParameterDefinition("START_MESSAGE", required = false),
                ParameterDefinition("END_MESSAGE", required = false)
            )
        )
    }

    val discordToken: String = parameters["DISCORD_TOKEN"]!!
    val channelId: Snowflake = Snowflake(parameters["CHANNEL_ID"]!!)
    val maxAge: String = parameters["MAX_AGE"]?.apply {
        if(!Regex("\\d+[dDwWmM]{1}").matches(this)) {
            log.warn("couldn't parse MAX_AGE Parameter '${this}'; Using Default: '${defaultMaxAge}'")
        }
    } ?: defaultMaxAge
    val startingMessage: String = parameters["START_MESSAGE"] ?: defaultStartingMessage
    val endMessage: String = parameters["END_MESSAGE"] ?: defaultEndMessage
}