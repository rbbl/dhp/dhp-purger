# DHP Purger

this is currently a small CLI utility to delete discord messages in a specific channel which are older than the
specified age

## usage

- create a Discord Bot
- add bot to server
    - Permissions: `Manage Messages`, `Read Message History`
- make sure the bot has access to the channel in question
-  ```shell
   docker run rbbl/dhp-purger CHANNEL_ID=channelId DISCORD_TOKEN=discordToken
   ```

### paramteres

| Parameter     | Required | Default |     
|---------------|----------|--------|
| CHANNEL_ID    | yes      |        |     
| DISCORD_TOKEN | yes      |        |
| MAX_AGE       | no       | 7d     |
